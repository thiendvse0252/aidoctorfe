import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../models/booking.dart';

class AdminHomeScreen extends StatefulWidget {
  const AdminHomeScreen({Key? key}) : super(key: key);

  @override
  State<AdminHomeScreen> createState() => _AdminHomeScreenState();
}

class _AdminHomeScreenState extends State<AdminHomeScreen> {
  List<Booking> reservations = [];
  DateTime selectedDate = DateTime.now();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Column(
        children: [
          const SizedBox(height: 40),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  onPressed: () {},
                  child: const Text(
                    'Select Date',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.blue,
                    ),
                  ),
                ),
                const SizedBox(width: 10),
                Text(
                  DateFormat('EEE, MMM dd, yyyy').format(selectedDate),
                  style: const TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 30,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: const [

              ],
            ),
          ),
          Expanded(
            child: RefreshIndicator(
              onRefresh: () async {
              },
              child: ListView.builder(
                itemCount: reservations.length,
                itemBuilder: (context, index) {
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
