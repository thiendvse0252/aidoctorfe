import 'package:flutter/material.dart';

import '../home/screens/HomeScreen.dart';
import 'AdminHomeScreen.dart';

class AdminScreenWrapper extends StatefulWidget {
  const AdminScreenWrapper({Key? key}) : super(key: key);

  @override
  State<AdminScreenWrapper> createState() => _AdminScreenWrapperState();
}

class _AdminScreenWrapperState extends State<AdminScreenWrapper> {
  int _currentIndex = 0;

  final List<Widget> _screens = [
    // Replace these with your actual screens/widgets
    const HomeScreen(),
    const AdminHomeScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}
