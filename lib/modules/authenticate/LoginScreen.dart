
import 'package:ai_doctor/constants/constant.dart';
import 'package:ai_doctor/modules/admin/AdminHomeScreen.dart';
import 'package:ai_doctor/modules/admin/AdminScreenWrapper.dart';
import 'package:ai_doctor/modules/authenticate/ForgotPasswordScreen.dart';
import 'package:ai_doctor/modules/home/screens/HomeScreen.dart';
import 'package:ai_doctor/utils/api_service.dart';
import 'package:ai_doctor/utils/shared_preferences.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;


import 'RegisterScreen.dart';
import '/widgets/stateful/overview.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  var _isVisible = false;
  @override
  Widget build(BuildContext context) {
    String username = "admin";
    String password = "123";
    final deviceHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SafeArea(
          child: SingleChildScrollView(

            child: Column(
              children: [
                const Overview()
                ,
                Container(
                  height: deviceHeight * 0.6,
                  width: double.infinity,
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  child: LayoutBuilder(builder: (ctx, constraints){
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: constraints.maxHeight * 0.01,),
                        Container(
                          height: constraints.maxHeight * 0.12,
                          decoration: BoxDecoration(
                            color: const Color(0xffffffff).withOpacity(0.4),
                            borderRadius: BorderRadius.circular(16),
                          ),

                          child: Padding(
                            padding: EdgeInsets.only(left: 15),
                            child: Center(
                              child: TextField(
                                decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Phone Number',
                                ),
                                onChanged: (value){
                                  setState(() {
                                    username = value;
                                  });
                                }),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: constraints.maxHeight * 0.02,
                        ),
                        Container(
                          height: constraints.maxHeight * 0.12,
                          decoration: BoxDecoration(
                            color: const Color(0xffffffff).withOpacity(0.4),
                            borderRadius: BorderRadius.circular(16),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Center(
                              child: TextField(
                                obscureText: _isVisible ? false: true,
                                decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    onPressed: (){
                                      setState(() {
                                        _isVisible = !_isVisible;
                                      });
                                    },
                                    icon: Icon(
                                      _isVisible
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  border: InputBorder.none,
                                  hintText: 'Password',
                                ),
                                  onChanged: (value){
                                    setState(() {
                                      password = value;
                                    });
                                  }),
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            TextButton(onPressed: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => const ForgotPasswordScreen()),
                              );

                            }, child: const Text(
                              'Forgot password?',
                              style: TextStyle(
                                color: Color(0xFF00DCA6),
                              ),
                            ))
                          ],
                        ),
                        Container(
                          width: double.infinity,
                          height: constraints.maxHeight * 0.12,
                          margin: EdgeInsets.only(
                            top: constraints.maxHeight * 0.05,

                          ),
                          child: ElevatedButton(
                            onPressed: (){
                              debugPrint("Username: $username");
                              debugPrint("Password: $password");
                              if(username == "" || password == ""){
                                showAlertDialog(
                                    context, "Error", "Username or password is empty");
                                return;
                              }else{
                                ApiService().post('/auth/login', {
                                  "username": username,
                                  "password": password,
                                }).then((data) {
                                  if(data['result'] == true){
                                    SharedPreferencesUtil.saveUserToken(data['data']['token']);
                                    // Check if the user is admin or not
                                    List<String> roles = data['data']['roles'].cast<String>();
                                    if (roles.contains("ROLE_ADMIN")) {
                                      SharedPreferencesUtil.saveIsAdmin(true);
                                      Navigator.of(context).pop();
                                      Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                              const AdminScreenWrapper()));
                                    } else {
                                      SharedPreferencesUtil.saveIsAdmin(false);
                                      Navigator.of(context).pop();
                                      Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => const HomeScreen()));
                                    }
                                  }

                                });
                              }

                            },
                            style: ElevatedButton.styleFrom(
                                primary: const Color(0xFF00DCA6),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(28),
                                )),
                            child: const Text(
                              'LOGIN',
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 22,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: constraints.maxHeight * 0.02,
                        ),
                        RichText(text: TextSpan(
                            text: 'New here? ',
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                            ),
                            children: [
                              TextSpan(
                                  text: 'Register',
                                  style: const TextStyle(
                                    color: Color(0xFF00DCA6),
                                    fontSize: 18,
                                  ),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => const RegisterScreen()),
                                      );
                                    }
                              )
                            ]
                        ))
                      ],
                    );
                  },),
                ),

              ],
            ),
          )
      ),
    );
  }

}


