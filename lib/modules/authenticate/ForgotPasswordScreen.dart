import 'package:ai_doctor/config/themes/app_colors.dart';
import 'package:ai_doctor/widgets/stateful/overview.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'LoginScreen.dart';

class ForgotPasswordScreen extends StatelessWidget {
  const ForgotPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final deviceHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const Overview(),
              Container(
                height: deviceHeight * 0.6,
                width: double.infinity,
                margin: const EdgeInsets.symmetric(horizontal: 20),
                child: LayoutBuilder(builder: (ctx, constraints){
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                      SizedBox(
                          height: constraints.maxHeight * 0.06),
                      const SizedBox(
                        width: 260,
                        child: Center(
                          child: Text('An OTP code will be sent to your device',

                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),

                          ),
                        ),
                      ),

                      SizedBox(
                          height: constraints.maxHeight * 0.10),

                      //enter phone number
                      Container(
                        height: constraints.maxHeight * 0.12,
                        decoration: BoxDecoration(
                          color: const Color(0xffffffff).withOpacity(0.4),
                          borderRadius: BorderRadius.circular(16),
                        ),

                        child: const Padding(
                          padding: EdgeInsets.only(left: 15),
                          child: Center(
                            child: TextField(
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Enter your phone number',
                              ),
                            ),
                          ),
                        ),
                      ),

                      Container(
                        width: double.infinity,
                        height: constraints.maxHeight * 0.12,
                        margin: EdgeInsets.only(
                          top: constraints.maxHeight * 0.05,

                        ),
                        child: ElevatedButton(
                          onPressed: (){
                          },
                          style: ElevatedButton.styleFrom(
                              primary: AppColor.brightgreen,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(28),
                              )),
                          child: const Text(
                            'GET OTP',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 22,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: constraints.maxHeight * 0.03,
                      ),
                      RichText(text: TextSpan(
                          children: [
                            TextSpan(
                                text: 'Return to Log In',
                                style: const TextStyle(
                                  color: Color(0xFF00DCA6),
                                  fontSize: 16,
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(builder: (context) => const LoginScreen()),
                                    );
                                  }
                            )
                          ]
                      ))
                    ],
                  );
                }),
              ),


            ],
          ),
        ),
      ),
    );
  }
}
