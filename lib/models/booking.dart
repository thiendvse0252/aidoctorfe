class Booking {
  int? id;
  int? userId;
  int? doctorId;
  DateTime? date;
  String? status;

  Reservation() {
    // TODO: implement Reservation
    throw UnimplementedError();
  }

  Booking.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['userId'];
    doctorId = json['tableId'];
    date = DateTime.parse(json['reservationDate']);
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'userId': userId,
      'tableId': doctorId,
      'reservationDate': date!.toIso8601String(),
      'status': status,
    };
  }
}