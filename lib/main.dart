import 'package:ai_doctor/modules/overview/screens/FirstScreen.dart';
import 'package:flutter/material.dart';

import 'config/themes/app_colors.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'AI Doctor',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(

        scaffoldBackgroundColor: AppColor.darkblue,
        useMaterial3: true,
      ),
      home: const FirstScreen(),
    );
  }
}

