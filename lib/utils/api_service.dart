import 'dart:convert';

import 'package:ai_doctor/utils/shared_preferences.dart';
import 'package:http/http.dart' as http;

class ApiService {
  static const String baseUrl = 'https://localhost:3000';

  Future<dynamic> get(String endpoint) async {
    String? token = await SharedPreferencesUtil.getUserToken();
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };
    if (token != null) {
      // set token to header
      final authorization = <String, String>{'Authorization': 'Bearer $token'};
      headers.addAll(authorization);
    }
    final response =
    await http.get(Uri.parse(baseUrl + endpoint), headers: headers);
    return _handleResponse(response);
  }

  Future<dynamic> post(String endpoint, dynamic data) async {
    String? token = await SharedPreferencesUtil.getUserToken();
    Map<String, String> headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };
    if (token != null) {
      // set token to header
      final authorization = <String, String>{'Authorization': 'Bearer $token'};
      headers.addAll(authorization);
    }
    final response = await http.post(
      Uri.parse(baseUrl + endpoint),
      body: jsonEncode(data),
      headers: headers,
    );
    return _handleResponse(response);
  }

  dynamic _handleResponse(http.Response response) {
    final responseBody = jsonDecode(response.body);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return responseBody;
    } else {
      throw Exception(
          'API request failed: ${response.statusCode} - ${responseBody['message']}');
    }
  }
}